#pragma once
#include "Unordered.h"

template<class T>
class Stack
{
public:
	Stack(int size) :mArray(NULL)
	{
		mArray = new UnorderedArray<T>(size);
	}
	virtual void push(T value)
	{
		mArray->push(value);
	}
	virtual void pop()
	{
		mArray->pop();
	}
	virtual const T& top()
	{
		return mArray[0][mArray->getSize() -1];
	}
	virtual void clear()
	{
		mArray->clear();
	}
	virtual int getSize()
	{
		return mArray->getSize();
	}
	virtual const T& operator[](int index) const
	{
		return mArray[0][index];
	}
private:
	UnorderedArray<T>*mArray;
};