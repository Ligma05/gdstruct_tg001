#include <iostream>
#include <string>
#include <conio.h>
#include "Queue.h"
#include "Stack.h"
using namespace std;

int main() {
	int size;
	int choice = 0;
	int number;
	cout << "What is the size of your Element: ";
	cin >> size;
	system("CLS");
	
	Queue <int> queue(size);
	Stack<int> stack(size);
	while (choice != 4) {
		cout << "What do you want to do? \n";
		cout << "1 - Push Elements \n";
		cout << "2 - Pop Element \n";
		cout << "3 - Print everything then empty set \n";
		cout << "4 - Exit \n";
		cin >> choice;
		
		if (choice == 1)
		{
			cout << "Input Number that will be pushed: ";
			cin >> number;
			queue.push(number);
			stack.push(number);
			cout << "Top Elements: \n";
			cout << "Queue Element: ";
			cout << queue.top() << endl;
			cout << "Stack Elements: ";
			cout << stack.top() << endl;
			_getch();
			system("CLS");
		}
		else if (choice == 2) {
			cout << "Queue Elements: \n";
			queue.pop();
			stack.pop();
			cout << "Top Elements: \n";
			cout << "Queue Element: ";
			cout << queue.top() << endl;
			cout << "Stack Elements: ";
			cout << stack.top() << endl;
			_getch();
			system("CLS");
		}
		else if (choice == 3) {
			cout << "Queue Elements: \n";
			for (int i = 0; i < queue.getSize(); i++) {
				cout << queue[i] << " ";
			}
			cout << "Stack Elements: \n";
			for (int i = 0; i < stack.getSize(); i++) {
				cout << stack[i] << " ";
			}
			_getch();
			cout << "Elements Deleted \n";
			for (int i = 0; i < size; i++) {
				stack.pop();
				queue.pop();
			}
			_getch();
			system("CLS");
		}
		else if (choice == 4) {
			cout << "you chose to exit! \n";
			_getch();
			return 0;
		}
	}
	system("pause");
	return 0;
};