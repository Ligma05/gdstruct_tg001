#pragma once
#include "Unordered.h"

template<class T>
class Queue
{
public:
	Queue(int size) :mArray(NULL)
	{
		mArray = new UnorderedArray<T>(size);
	}
	virtual void push(T value)
	{
		mArray->push(value);
	}
	virtual void pop()
	{
		for (int i = 0; i < mArray->getSize(); i++)
		{
			mArray[0][i] = mArray[0][i + 1];
		}
		mArray->pop();
	}
	virtual const T& top()
	{
		return mArray[0][0];
	}
	virtual void clear()
	{
		mArray->clear();
	}
	virtual int getSize()
	{
		return mArray->getSize();
	}
	virtual const T& operator[](int index)const
	{
		return mArray[0][index];
	}
private:
	UnorderedArray<T>*mArray;

};


