#include <iostream>
#include <string>
#include <vector>
#include "UnorderedArray.h"
#include <time.h>
using namespace std;

void main() {
	srand(time(NULL));
	int size;
	int choice;
	UnorderedArray<int> UA(5);
	cout << "how many elements do you want for you array: \n";
	cin >> size;
	system("pause");

	for (int i = 1; i <= size; i++) {
		int randomNum = rand() % 100 + 1;
		UA.push(randomNum);
	}
	

	for (int i = 0; i < UA.getSize();i++) {
		cout << UA[i] << endl;
	}

	cout << "What Number do you want to remove \n";
	cout << "Type the index number \n";
	cin >> choice;

	cout << "Element Removed \n";
	UA.remove(choice - 1);

	system("pause");
	system("CLS");
	for (int i = 0; i < UA.getSize();i++) {
		cout << UA[i] << endl;
	}
	cout << "What Number do you want to search in your array: \n";
	cin >> choice;
	UA.linearSearch(choice);

	system("pause");
}
