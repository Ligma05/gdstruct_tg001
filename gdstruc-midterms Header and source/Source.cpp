#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void main()
{
	srand(time(NULL));
	int index;
	int choice = 0;
	cout << "Enter size of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}
	while (choice > 0 || choice < 4)
	{
		cout << "\nGenerated array: " << endl;
		cout << "Unordered: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << "   ";

		cout << "\nOrdered: ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << "   ";

		cout << "\nWhat do you want to do? \n";
		cout << "1 - Remove Element at Index\n";
		cout << "2 - Search for element \n";
		cout << "3 - Expand and generate random values \n";
		cin >> choice;
		if (choice == 1) {
			cout << "\nType index of the element to remove: \n";
			cin >> index;
			unordered.remove(index);
			cout << "Successfully remove \n";
		}

		if (choice == 2) {
			cout << "\n\nEnter number to search: ";
			int input;
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

			cout << "Ordered Array (binary Search): \n";
			ordered.binarySearch(input);
			
			
		}
		
	}
}