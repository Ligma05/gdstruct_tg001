#include <iostream>
#include <string>

using namespace std;

void fibonacci(int n, int current, int previous)
{
	int temp;
	if (n <= 0) 
	{
		return;
	}
	previous = 1;
	current = 0;
	temp = previous + current;
	previous = current;
	current = temp;
	fibonacci(n, current, previous);

}

void printNumbersReversalFrom(int from, int sum)
{
	if (from <= 0 ) {
		cout << "Total : " << sum << endl;
		return;

	}
	cout << from << " ";
	
	printNumbersReversalFrom(from - 1, sum);
	
}
void printInputNumbers(int input, int sum)
{	
	
	int i = input / 10;
	float f = (float)input / 10;

	f = f - i;
	f = f * 10;
	cout << (int)f << endl;
	sum = sum + f;
	system("pause");
	if (f == 0) {
		system("CLS");
		cout << "total: " << sum << endl;
		return;
	}
	printInputNumbers(i, sum);

}

void main()
{
	int size = 0;
	int input;

	cout << "Input : " << endl;
	cin >> input;

	printInputNumbers(input, 0);

	system("pause");
	system("CLS");

	cout << "Enter the number of terms: ";
	cin >> size;
	cout << "Fibonacci Series: ";
	fibonacci(size, 0, 0);

	cout << "\n";
	system("pause");

}